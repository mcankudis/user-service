"use strict";
import * as mongoose from 'mongoose';
const dbpath = process.env.DB_PATH || 'mongodb://localhost/userservice';

mongoose.connect(dbpath, {
  user: process.env.DB_USER,
  pass: process.env.DB_PASS,
  useNewUrlParser: true,
  useCreateIndex: true
}, err => {
    if(err) console.error(`[${new Date}, db/connect]`, err);
    else console.log(new Date, `Nawiązano połączenie z bazą`);
  }
)

export default mongoose;
