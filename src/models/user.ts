"use strict";
import db from '../db';
import * as bcrypt from 'bcryptjs';

export type UserDocument = db.Document & {
	username: string;
	password: string;
	email: string;
  reissue_id: string;
  password_reset_id: string;
  changePassword: changePasswordFunction;
  changeUsername: changeUsernameFunction;
  comparePassword: comparePasswordFunction;
  createUser: createUserFunction;
  getById: getByIdFunction;
  getByUsername: getByUsernameFunction;
};

type changePasswordFunction = (id: string, password: string) => Promise<object>;
type changeUsernameFunction = (id: string, username: string) => Promise<object>;
type comparePasswordFunction = (password: string, userPassword: string) => Promise<boolean>;
type createUserFunction = (newUser: newUser) => Promise<object>;
type getByIdFunction = (id: string) => Promise<UserDocument>;
type getByUsernameFunction = (username: string) => Promise<UserDocument>;

const userSchema = new db.Schema({
  username:   {type: String, required: true, unique: true, index: true},
  password:   {type: String, required: true},
  email:      {type: String, required: true, unique: true, index: true},
  reissue_id: {type: String, required: false},
  password_reset_id:   {type: String, required: false}
}, { timestamps: true });

interface newUser extends db.Document {
  username: string;
  password: string;
  email: string;
}

const getByUsername = async(username: string): Promise<object> => {
  try {
    const user = await User.findOne({username: username});
    if(!user) throw {msg: 'User Not Found'};
    return user;
  }
  catch(err) {
    console.error('[models/user/getByUsername]', err);
    throw err;
  }
}
const changePassword: changePasswordFunction = async(id, password) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    const res = await User.updateOne({_id: id}, {$set: {password: hash}});
    return res;
  }
  catch(err) {
    console.log(err);
    throw err;
  }
}
const changeUsername: changeUsernameFunction = async(id, username) => {
  try {
    const res = await User.updateOne({_id: id}, {$set: {username: username}});
    return res;
  }
  catch(err) {
    console.log(err);
    throw err;
  }
}
const comparePassword: comparePasswordFunction = async(password, userPassword) => {
  try {
    const isMatch = await bcrypt.compare(password, userPassword);
    if(!isMatch) return false;
    return true;
  }
  catch(err) {
    console.log(err);
    throw err;
  }
}
const createUser: createUserFunction = async(newUser) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(newUser.password, salt);
    newUser.password = hash;
    const user = await newUser.save();
    return user;
  }
  catch(err) {
    console.error('[models/user/createUser]', err);
    throw err;
  }
}
const getById: getByIdFunction = async(id) => {
  try {
    const user = await User.findOne({_id: id});
    if(!user) throw {msg: 'User Not Found'};
    return user;
  }
  catch(err) {
    console.error('[models/user/getById]', err);
    throw err;
  }
}

userSchema.methods.comparePassword = comparePassword;
userSchema.methods.changeUsername = changeUsername;
userSchema.methods.comparePassword = comparePassword;
userSchema.methods.createUser = createUser;
userSchema.methods.getById = getById;
userSchema.methods.getByUsername = getByUsername;

const user = db.model<UserDocument>("User", userSchema);
export const User = Object.assign(user, {
  changePassword: changePassword,
  changeUsername: changeUsername,
  comparePassword: comparePassword,
  createUser: createUser,
  getById: getById,
  getByUsername: getByUsername
})
