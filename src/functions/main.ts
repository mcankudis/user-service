import sanitize from '../functions/sanitize';
import * as jwt from 'jsonwebtoken';
import { UserDocument } from "../models/user";

export const getRandomString = (mini: number, maxi: number) => {
  let tmp = Math.ceil(Math.random()*(maxi-mini))+mini;
  let helper = "1234567890abcdefghijklmnopqrstuvwxyz";
  let string = '';
  for (let i = 0; i < tmp; i++) {
    let rand = Math.floor(Math.random()*30);
    string+=helper[rand];
  }
  return string;
}

export const login = (async(username: string, password: string, userDependency: UserDocument) => {
  try {
    if(!username || !password) throw {msg: 'Missing Credentials'}
    if(!sanitize.verifyString(username)) throw {msg: 'Invalid Data'};
    if(!sanitize.verifyString(password)) throw {msg: 'Invalid Data'};
    const user = await userDependency.getByUsername(username);
    const isMatch = await userDependency.comparePassword(password, user.password);
    if(!isMatch) throw {msg:'Wrong Password'};
    return user;
  }
  catch(err) {
    console.error('[FUNCTIONS/MAIN/LOGIN]', err);
    throw err;
  }
});
