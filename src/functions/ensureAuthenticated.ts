import sanitize from './sanitize';
import * as jwt from 'jsonwebtoken';
import { Req, Res, Auth } from "../types";

const ensureAuthenticated = (req: Req, res: Res, next: Function) => {
  if(!req.headers['x-auth'] || !sanitize.verifyString(req.headers['x-auth']))
    return res.status(403).send("Missing credentials");
  jwt.verify(req.headers['x-auth'], process.env.SECRET, {algorithms: ['HS512']}, function(err, token: Auth) {
    if(err) {
      console.error(`[${new Date}, functions/ensureAuthenticated]`, err);
      return res.status(403).send("Permission denied");
    }
    req.auth = token;
    return next();
  });
}

export default ensureAuthenticated;
