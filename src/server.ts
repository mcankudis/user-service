'use strict';
import * as dotenv from 'dotenv';
dotenv.config();
import * as express from 'express';
const app = express();
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';

// Security headers
app.use(helmet());

// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Static folder
app.use(express.static(__dirname + '/client'));

// View Engine
app.set('views', __dirname + '/client/views');
app.set('view engine', 'pug');

// These are the routes
import mainRoute from './routes/index';
import userRoute from './routes/user';
app.use('/', mainRoute);
app.use('/user', userRoute);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(new Date, `Hi, listening on port ${port}`);
})
