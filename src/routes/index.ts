'use strict';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import sanitize from '../functions/sanitize';
import { Req, Res, Auth } from '../types';
const router = express.Router();

router.get('/', (req: Req, res: Res) => {
  res.render('index');
})

router.get('/profile', (req: Req, res: Res) => {
  res.render('profile');
})

router.get('/login', (req: Req, res: Res) => {
  res.render('login');
})

router.get('/register', (req: Req, res: Res) => {
  res.render('register');
})

router.get('/reset/:token', (req: Req, res: Res) => {
  if(!req.params.token || !sanitize.verifyString(req.params.token))
    return res.status(403).send("Missing credentials");
    jwt.verify(req.params.token, process.env.PASSWORD_RESET_SECRET, (err, token: Auth) => {
    if(err) {
      console.error(`[${new Date}, /reset/:token]`, err);
      return res.status(403).send("Permission denied");
    }
    if(token.username) res.render('resetpassword', {token: req.params.token});
    else res.status(403).send("Missing credentials");
  });
})

export default router;
