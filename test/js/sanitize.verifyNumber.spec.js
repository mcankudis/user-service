"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const expect = chai.expect;
const sanitize_1 = require("../../js/functions/sanitize");
const v = sanitize_1.default.verifyNumber;
describe('valid numbers - expect true', () => {
    it('20 (number)', () => {
        let n = 20;
        return expect(v(n)).to.equal(true);
    });
    it('20 (string)', () => {
        let n = '20';
        return expect(v(n)).to.equal(true);
    });
    it('2029383987453453459871934648098812348732463287463274632874 (number)', () => {
        let n = 2029383987453453459871934648098812348732463287463274632874;
        return expect(v(n)).to.equal(true);
    });
    it('2029383987453453459871934648098812348732463287463274632874 (string)', () => {
        let n = '2029383987453453459871934648098812348732463287463274632874';
        return expect(v(n)).to.equal(true);
    });
    it('-555 (number)', () => {
        let n = -555;
        return expect(v(n)).to.equal(true);
    });
    it('-555 (string)', () => {
        let n = "-555";
        return expect(v(n)).to.equal(true);
    });
    it('-10.5 (number)', () => {
        let n = 10.5;
        return expect(v(n)).to.equal(true);
    });
    it('-10.5 (string)', () => {
        let n = "10.5";
        return expect(v(n)).to.equal(true);
    });
});
describe('invalid numbers - expect false', () => {
    it('2a', () => {
        let n = "2a";
        return expect(v(n)).to.equal(false);
    });
    it('2(0', () => {
        let n = '2(0';
        return expect(v(n)).to.equal(false);
    });
    it('10,6', () => {
        let n = "10,6";
        return expect(v(n)).to.equal(false);
    });
});
