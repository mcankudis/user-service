"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const expect = chai.expect;
const sanitize_1 = require("../../js/functions/sanitize");
const v = sanitize_1.default.verifyString;
describe('invalid "strings" - expected false', () => {
    it('$set', () => {
        let string = "$set";
        return expect(v(string)).to.equal(false);
    });
    it('http://virus', () => {
        let string = "http://virus";
        return expect(v(string)).to.equal(false);
    });
    it('https://virus', () => {
        let string = "https://virus";
        return expect(v(string)).to.equal(false);
    });
    it('http://virus', () => {
        let string = "http://virus";
        return expect(v(string)).to.equal(false);
    });
    it('db.collection.remove({})', () => {
        let string = "db.collection.remove({})";
        return expect(v(string)).to.equal(false);
    });
    it('text; mallicious db query', () => {
        let string = "text; mallicious db query";
        return expect(v(string)).to.equal(false);
    });
});
describe('valid "strings" - expected true', () => {
    it('hello', () => {
        let string = "hello";
        return expect(v(string)).to.equal(true);
    });
    it('Random http thrown into some text', () => {
        let string = "Random http thrown into some text";
        return expect(v(string)).to.equal(true);
    });
    it('A_valid-string.', () => {
        let string = "Random http thrown into some text";
        return expect(v(string)).to.equal(true);
    });
    it('Another,valid*string^', () => {
        let string = "Random http thrown into some text";
        return expect(v(string)).to.equal(true);
    });
});
