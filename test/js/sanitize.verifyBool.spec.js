"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const expect = chai.expect;
const sanitize_1 = require("../../js/functions/sanitize");
const v = sanitize_1.default.verifyBool;
describe('valid booleans - expect true', () => {
    it('true', () => {
        let n = true;
        return expect(v(n)).to.equal(true);
    });
    it('false', () => {
        let n = false;
        return expect(v(n)).to.equal(true);
    });
});
describe('invalid booleans - expect false', () => {
    it('1', () => {
        let n = 1;
        return expect(v(n)).to.equal(false);
    });
    it('0', () => {
        let n = 0;
        return expect(v(n)).to.equal(false);
    });
    it('empty string', () => {
        let n = "";
        return expect(v(n)).to.equal(false);
    });
    it("string", () => {
        let n = "x";
        return expect(v(n)).to.equal(false);
    });
    it("empty array", () => {
        let n = [];
        return expect(v(n)).to.equal(false);
    });
    it("array", () => {
        let n = [true];
        return expect(v(n)).to.equal(false);
    });
    it('empty object', () => {
        let n = {};
        return expect(v(n)).to.equal(false);
    });
    it('object', () => {
        let n = { true: true };
        return expect(v(n)).to.equal(false);
    });
    it('function', () => {
        let n = function () { return true; };
        return expect(v(n)).to.equal(false);
    });
});
