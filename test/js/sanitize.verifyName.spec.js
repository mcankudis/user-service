"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai = require("chai");
const expect = chai.expect;
const sanitize_1 = require("../../js/functions/sanitize");
const v = sanitize_1.default.verifyName;
describe('valid firstNames - expect true', () => {
    it('Jan', () => {
        let firstName = "Jan";
        return expect(v(firstName)).to.equal(true);
    });
    it('Łukasz', () => {
        let firstName = "Łukasz";
        return expect(v(firstName)).to.equal(true);
    });
    it('Mikołaj', () => {
        let firstName = "Mikołaj";
        return expect(v(firstName)).to.equal(true);
    });
    it('Bożydar', () => {
        let firstName = "Bożydar";
        return expect(v(firstName)).to.equal(true);
    });
    it('Józef', () => {
        let firstName = "Józef";
        return expect(v(firstName)).to.equal(true);
    });
    it("El'sha", () => {
        let firstName = "A'b";
        return expect(v(firstName)).to.equal(true);
    });
});
describe('invalid firstNames - expect false', () => {
    it('illegal sign "', () => {
        let firstName = 'Ja"n';
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign ;', () => {
        let firstName = "Ja;n";
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign &', () => {
        let firstName = "Ja&n";
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign *', () => {
        let firstName = "Ja*n";
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign .', () => {
        let firstName = "Ja.n";
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign ↓', () => {
        let firstName = "Ja↓n";
        return expect(v(firstName)).to.equal(false);
    });
    it('illegal sign ŋ', () => {
        let firstName = "Ja↓n";
        return expect(v(firstName)).to.equal(false);
    });
});
describe('valid lastNames - expect true', () => {
    it('Cieślak', () => {
        let firstName = "Cieślak";
        return expect(v(firstName)).to.equal(true);
    });
    it('Górski', () => {
        let firstName = "Górski";
        return expect(v(firstName)).to.equal(true);
    });
    it('Łukaszewski', () => {
        let firstName = "Łukaszewski";
        return expect(v(firstName)).to.equal(true);
    });
    it('Źdźbło', () => {
        let firstName = "Źdźbło";
        return expect(v(firstName)).to.equal(true);
    });
    it('Iksińska-Kowalska', () => {
        let firstName = "Iksińska-Kowalska";
        return expect(v(firstName)).to.equal(true);
    });
    it('Iksińska - Kowalska', () => {
        let firstName = "Iksińska - Kowalska";
        return expect(v(firstName)).to.equal(true);
    });
    it("O'Connor", () => {
        let firstName = "O'connor";
        return expect(v(firstName)).to.equal(true);
    });
});
