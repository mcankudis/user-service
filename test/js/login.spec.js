"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("../../js/functions/main");
const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const expect = chai.expect;
chai.use(chaiAsPromised);
describe('Basic tests', () => {
    it('Returns an object with property username', () => __awaiter(this, void 0, void 0, function* () {
        const User = {
            getByUsername: (username) => __awaiter(this, void 0, void 0, function* () {
                return { username };
            }),
            comparePassword: () => true
        };
        return expect(main_1.login('test', 'empty', User)).to.eventually.have.property('username');
    }));
    it('Calls userDependency.getByUsername with proper username', () => __awaiter(this, void 0, void 0, function* () {
        const User = {
            getByUsername: (username) => __awaiter(this, void 0, void 0, function* () {
                expect(username).to.equal('test');
                return {
                    password: 'something'
                };
            }),
            comparePassword: (password, password2) => __awaiter(this, void 0, void 0, function* () { return true; })
        };
        yield main_1.login('test', 'empty', User);
    }));
    it('Calls userDependency.comparePassword with proper password', () => __awaiter(this, void 0, void 0, function* () {
        const User = {
            getByUsername: (username) => __awaiter(this, void 0, void 0, function* () {
                return {
                    password: 'something'
                };
            }),
            comparePassword: (password, password2) => __awaiter(this, void 0, void 0, function* () {
                expect(password).to.equal('empty');
                return true;
            })
        };
        yield main_1.login('test', 'empty', User);
    }));
    it('Calls userDependency.comparePassword with proper user.password', () => __awaiter(this, void 0, void 0, function* () {
        const User = {
            getByUsername: (username) => __awaiter(this, void 0, void 0, function* () {
                return {
                    password: 'something'
                };
            }),
            comparePassword: (password, password2) => __awaiter(this, void 0, void 0, function* () {
                expect(password2).to.equal('something');
                return true;
            })
        };
        yield main_1.login('test', 'empty', User);
    }));
});
