"use strict";
import * as chai from 'chai';
const expect = chai.expect;
import f from '../../js/functions/sanitize';
const v = f.verifyEmail;

describe('valid emails - expect true', () => {
    it('test@test.com', () => {
        let email = "test@test.com";
        return expect(v(email)).to.equal(true);
    });
    it('john.smith@gmail.com', () => {
        let email = "john.smith@gmail.com";
        return expect(v(email)).to.equal(true);
    }),
        it('johnny209@o2.de', () => {
            let email = "johnny209@o2.de";
            return expect(v(email)).to.equal(true);
        }),
        it('ann-765@gone_with_the_wind.io', () => {
            let email = "ann-765@gone_with_the_wind.io";
            return expect(v(email)).to.equal(true);
        });
    it('ANN24_-123@onet.pl', () => {
        let email = "ANN24_-123@some-site.pl";
        return expect(v(email)).to.equal(true);
    });
});
describe('invalid emails - expect flase', () => {
    it('illegal sign $', () => {
        let email = "te$st@test.com";
        return expect(v(email)).to.equal(false);
    });
    it('illegal sign ;', () => {
        let email = "te;st@test.com";
        return expect(v(email)).to.equal(false);
    }),
        it('illegal sign ,', () => {
            let email = "te,st@test.com";
            return expect(v(email)).to.equal(false);
        });
    it('no @', () => {
        let email = "testtest.com";
        return expect(v(email)).to.equal(false);
    });
    it('double @', () => {
        let email = "te@st@test.com";
        return expect(v(email)).to.equal(false);
    });
    it('no .', () => {
        let email = "test@test";
        return expect(v(email)).to.equal(false);
    });
});
