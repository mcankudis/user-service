'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const helmet = require("helmet");
// Security headers
app.use(helmet());
// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Static folder
app.use(express.static(__dirname + '/client'));
// View Engine
app.set('views', __dirname + '/client/views');
app.set('view engine', 'pug');
// These are the routes
const index_1 = require("./routes/index");
const user_1 = require("./routes/user");
app.use('/', index_1.default);
app.use('/user', user_1.default);
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(new Date, `Hi, listening on port ${port}`);
});
