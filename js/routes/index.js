'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const jwt = require("jsonwebtoken");
const sanitize_1 = require("../functions/sanitize");
const router = express.Router();
router.get('/', (req, res) => {
    res.render('index');
});
router.get('/profile', (req, res) => {
    res.render('profile');
});
router.get('/login', (req, res) => {
    res.render('login');
});
router.get('/register', (req, res) => {
    res.render('register');
});
router.get('/reset/:token', (req, res) => {
    if (!req.params.token || !sanitize_1.default.verifyString(req.params.token))
        return res.status(403).send("Missing credentials");
    jwt.verify(req.params.token, process.env.PASSWORD_RESET_SECRET, (err, token) => {
        if (err) {
            console.error(`[${new Date}, /reset/:token]`, err);
            return res.status(403).send("Permission denied");
        }
        if (token.username)
            res.render('resetpassword', { token: req.params.token });
        else
            res.status(403).send("Missing credentials");
    });
});
exports.default = router;
