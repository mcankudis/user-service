'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const ensureAuthenticated_1 = require("../functions/ensureAuthenticated");
const router = express.Router();
// DB Models
const user_1 = require("../models/user");
// Middleware/functions
const sanitize_1 = require("../functions/sanitize");
const main_1 = require("../functions/main");
router.post('/register', (req, res) => {
    let email = req.body.email;
    let username = req.body.username;
    let password = req.body.password;
    if (!sanitize_1.default.verifyString(username) || !username)
        return res.status(400).send("Invalid username");
    if (!sanitize_1.default.verifyString(password) || !password)
        return res.status(400).send("Invalid password");
    if (!sanitize_1.default.verifyEmail(email) || !email)
        return res.status(400).send("Invalid email");
    let newUser = new user_1.User({
        email: email,
        username: username,
        password: password,
        reissue_id: main_1.getRandomString(20, 30)
    });
    user_1.User.createUser(newUser)
        .then(() => res.status(200).send("Account created"))
        .catch(err => {
        if (err.code == "11000") {
            if (err.errmsg.includes("email"))
                return res.status(400).send("Email already taken");
            return res.status(400).send("Username already taken");
        }
        else
            return res.status(500).send("Unknown error");
    });
});
router.post('/login', (req, res) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (!sanitize_1.default.verifyString(req.body.username))
            throw { msg: "Invalid data" };
        if (!sanitize_1.default.verifyString(req.body.password))
            throw { msg: "Invalid data" };
        let user = yield main_1.login(req.body.username, req.body.password, new user_1.User);
        let random = main_1.getRandomString(40, 60);
        let token = jwt.sign({
            username: user.username,
            id: user._id,
            random: random
        }, process.env.SECRET, { algorithm: 'HS512', expiresIn: '10m' });
        random = main_1.getRandomString(40, 60);
        let reissueToken = jwt.sign({
            id: user._id,
            random: random,
            reissue_id: user.reissue_id
        }, process.env.REISSUE_SECRET, { algorithm: 'HS512', expiresIn: '7d' });
        res.json({
            token,
            reissueToken,
            user: {
                username: user.username,
                _id: user._id
            }
        });
    }
    catch (err) {
        console.error(`[${new Date}, /user/login]`, err);
        if (err.msg)
            return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
}));
router.get('/id/:id', (req, res) => {
    let id = req.params.id;
    if (!sanitize_1.default.verifyWord(id))
        return res.status(400).send("Invalid data");
    user_1.User.findOne({ _id: id }).select('username').select('_id').select('reissue_id').exec((err, user) => {
        if (err)
            return res.sendStatus(500);
        if (!user)
            return res.sendStatus(404);
        res.json(user);
    });
});
router.get('/username/:name', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let username = req.params.name;
    if (!sanitize_1.default.verifyString(username))
        return res.status(400).send("Invalid data");
    user_1.User.findOne({ username: username }).select('username').select('_id').select('reissue_id').exec((err, user) => {
        if (err)
            return res.sendStatus(404);
        if (!user)
            return res.sendStatus(404);
        res.json(user);
    });
}));
router.post('/password/change', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (!req.body.password || !req.body.newPassword)
            throw { msg: "Missing data" };
        if (!sanitize_1.default.verifyString(req.body.password))
            throw { msg: "Invalid data" };
        if (!sanitize_1.default.verifyString(req.body.newPassword))
            throw { msg: "Invalid data" };
        let user = yield main_1.login(req.auth.username, req.body.password, new user_1.User);
        let result = yield user_1.User.changePassword(user._id, req.body.newPassword);
        console.log(result);
        return res.status(200).send("Password changed");
    }
    catch (err) {
        console.error(`[${new Date}, /USER/PASSWORD/CHANGE]`, err);
        if (err.msg)
            return res.status(400).send(err.msg);
        res.sendStatus(500);
    }
}));
router.post('/username/change', ensureAuthenticated_1.default, (req, res) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (!req.body.password || !req.body.newUsername)
            throw { msg: "Missing data" };
        if (!sanitize_1.default.verifyString(req.body.password))
            return res.status(400).send("Invalid data");
        if (!sanitize_1.default.verifyString(req.body.newUsername))
            return res.status(400).send("Invalid data");
        let user = yield main_1.login(req.auth.username, req.body.password, new user_1.User);
        let result = yield user_1.User.changeUsername(user._id, req.body.newUsername);
        console.log(result);
        let random = main_1.getRandomString(40, 60);
        let token = jwt.sign({
            username: req.body.newUsername,
            id: user._id,
            random: random
        }, process.env.SECRET, { algorithm: 'HS512', expiresIn: '10m' });
        return res.status(200).json({ username: req.body.newUsername, token });
    }
    catch (err) {
        console.error(`[${new Date}, /USER/USERNAME/CHANGE]`, err);
        if (err.code === 11000)
            err.msg = "Username already taken";
        if (err.msg)
            return res.status(400).send(err.msg);
        res.sendStatus(500);
    }
}));
router.post('/password/email', (req, res) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (!sanitize_1.default.verifyString(req.body.email))
            return res.status(400).send("Invalid data");
        let user = yield user_1.User.findOne({ email: req.body.email });
        if (!user)
            return res.status(404).send("Account not found");
        let random = main_1.getRandomString(40, 100);
        let password_reset_id = main_1.getRandomString(40, 100);
        let token = jwt.sign({ username: user.username, id: user._id, random, password_reset_id }, process.env.PASSWORD_RESET_SECRET, { expiresIn: '5m' });
        let result = yield user_1.User.updateOne({ _id: user._id }, { $set: { password_reset_id: password_reset_id } });
        if (!result.ok)
            throw { code: 500, msg: "Internal error" };
        let transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: 587,
            secure: false,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD
            },
            tls: {
                rejectUnauthorized: false
            }
        });
        let text = `<b>Hello ${user.username}</b>
      <p>Someone, probably You, requested a password reset for Your potikreff account.
        Click the link below to enter a new password. Please notice that the link is only active for 5 minutes.</p>
      <a href="${process.env.SERVER_PATH}/reset/${token}">Password reset</a>
      <p>In case it was not You who requested a password reset, please contact our support at
        <a href="mailto:mikolaj@cankudis.net">mikolaj@cankudis.net</a></p>`;
        let subject = "Password reset";
        let mailOptions = {
            from: '"noreply" <noreply@accounts.potikreff.pl>',
            to: req.body.email,
            subject: subject,
            html: text
        };
        let transport = yield transporter.sendMail(mailOptions);
        console.log(`[${new Date}, transport]`, transport);
        res.sendStatus(200);
    }
    catch (err) {
        console.error(`[${new Date}, /token/resetPassword]`, err);
        if (err.msg)
            return res.status(400).send(err.msg);
        res.sendStatus(500);
    }
}));
router.post('/password/reset', (req, res) => __awaiter(this, void 0, void 0, function* () {
    try {
        console.log(req.body);
        if (!sanitize_1.default.verifyString(req.body.password) || !req.body.password)
            return res.status(400).send("Invaid data");
        if (!sanitize_1.default.verifyString(req.body.token) || !req.body.token)
            return res.status(400).send("Invaid data");
        let tokenData = yield jwt.verify(req.body.token, process.env.PASSWORD_RESET_SECRET);
        if (typeof tokenData === 'string')
            throw { msg: "Missing credentials" };
        let user = yield user_1.User.getById(tokenData.id);
        if (user.password_reset_id !== tokenData.password_reset_id)
            return res.status(400).send("Invaid data");
        let result = yield user_1.User.changePassword(tokenData.id, req.body.password);
        console.log(`[${new Date}, result]`, result);
        result = yield user_1.User.updateOne({ _id: user._id }, { $set: { password_reset_id: '' } });
        console.log(`[result2]`, result);
        res.status(200).send("Password changed");
    }
    catch (err) {
        console.error(`[${new Date}, /user/password/reset]`, err);
        if (err.msg)
            return res.status(400).send(err.msg);
        res.sendStatus(500);
    }
}));
router.get('/token/reissue', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!sanitize_1.default.verifyToken(req.headers['x-reissue']))
        return res.status(400).send("Invalid authorization data");
    if (!req.headers['x-reissue'])
        return res.status(400).send("Invalid authorization data");
    try {
        let tokenData = yield jwt.verify(req.headers['x-reissue'], process.env.REISSUE_SECRET, { algorithms: ['HS512'] });
        if (typeof tokenData === 'string')
            throw { code: 403, msg: "Missing credentials" };
        let user = yield user_1.User.getById(tokenData.id);
        if (!user)
            throw { code: 404, msg: "User not found" };
        if (tokenData.reissue_id !== user.reissue_id)
            return res.status(400).send("Invalid authorization data");
        let random = main_1.getRandomString(40, 60);
        let token = jwt.sign({
            username: user.username,
            id: user._id,
            random: random
        }, process.env.SECRET, { algorithm: 'HS512', expiresIn: '10m' });
        random = main_1.getRandomString(40, 60);
        let reissueToken = jwt.sign({
            id: user._id,
            random: random,
            reissue_id: user.reissue_id
        }, process.env.REISSUE_SECRET, { algorithm: 'HS512', expiresIn: '7d' });
        res.json({ token, reissueToken });
    }
    catch (err) {
        console.error(`[${new Date}, /user/token/reissue]`, err);
        if (err.msg)
            return res.status(err.code).send(err.msg);
        res.sendStatus(500);
    }
}));
exports.default = router;
