"use strict";
const user = {username: localStorage.getItem('username'), id: localStorage.getItem('_id')};
const token = localStorage.getItem('token');
const reissue_token = localStorage.getItem('reissueToken');
if(!token) window.location.href = '/login';
document.getElementById('usernameDisplay').innerHTML = user.username;

const changeUsername = (username, password) => {
  const formData = {newUsername: username, password};
  fetch("/user/username/change", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "x-auth": token
    },
    referrer: "no-referrer",
    body: JSON.stringify(formData)
  })
  .then(async(res) => {
    if(!res.ok) res.text().then(text => {
      if(res.status===403) reissueToken(changeUsername(username, password))
      else toast(text, 4000, {backgroundColor: 'red'});
    });
    else {
      let body = await res.json();
      toast('Username changed', 4000, {backgroundColor: '#9ccc65'});
      user.username = body.username;
      document.getElementById('usernameDisplay').innerHTML = body.username;
      localStorage.setItem('username', body.username);
      localStorage.setItem('token', body.token);
    }
  });
}

const changePassword = (newPassword, password) => {
  const formData = {newPassword, password}
  fetch("/user/password/change", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
        "x-auth": token
    },
    referrer: "no-referrer",
    body: JSON.stringify(formData)
  })
  .then(async(res) => {
    if(!res.ok) res.text().then(text => {
      if(res.status===403) reissueToken(changeUsername(newPassword, password));
      else toast(text, 4000, {backgroundColor: 'red'})
    });
    else {
      toast('Password changed', 4000, {backgroundColor: '#9ccc65'});
    }
  });
}

const reissueToken = (callback) => {
  fetch('/user/token/reissue', {
    method: "GET",
    headers: {
        "Content-Type": "application/json",
        "x-auth": token,
        "x-reissue": reissueToken
    },
    referrer: "no-referrer"
  })
  .then(async(res) => {
    if(!res.ok) res.text().then(text => {
      localStorage.clear();
      window.localStorage.clear();
      window.location.href="/login";
      toast(text, 4000, {backgroundColor: 'red'});
    });
    else {
      let body = await res.json();
      localStorage.setItem('token', body.token);
      localStorage.setItem('reissueToken', body.reissueToken);
      token = body.token;
      reissue_token = body.reissueTtoken;
      callback();
    }
  });
}

const usernameForm = document.getElementById('usernameForm');
usernameForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const username = document.getElementById('newUsername').value;
  const password = document.getElementById('password').value;
  changeUsername(username, password);
})

const passwordForm = document.getElementById('passwordForm');
passwordForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const newPassword = document.getElementById('newPassword').value;
  const password = document.getElementById('password2').value;
  changePassword(newPassword, password);
})
