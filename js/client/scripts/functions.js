const toast = (msg, time, style) => {
  if(!time) time = 4000;
  let backgroundColor, color, fontSize;
  if(style) {
    backgroundColor = style.backgroundColor,
    color = style.color, fontSize = style.fontSize;
  }
  const toast = document.createElement('div');
  toast.style.backgroundColor = backgroundColor || '#424242';
  toast.style.display = 'inline-block';
  toast.style.position = 'fixed';
  toast.style.right = '10px';
  toast.style.top = '10px';
  toast.style.padding = '10px';
  toast.style.color = color || 'white';
  toast.style.fontSize = fontSize || '1.3rem';
  toast.style.borderRadius = '4px';
  toast.innerHTML = msg;
  const body = document.getElementsByTagName('body');
  body[0].appendChild(toast);
  setTimeout(()=>{
    toast.remove();
  }, time)
}
