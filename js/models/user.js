"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("../db");
const bcrypt = require("bcryptjs");
const userSchema = new db_1.default.Schema({
    username: { type: String, required: true, unique: true, index: true },
    password: { type: String, required: true },
    email: { type: String, required: true, unique: true, index: true },
    reissue_id: { type: String, required: false },
    password_reset_id: { type: String, required: false }
}, { timestamps: true });
const getByUsername = (username) => __awaiter(this, void 0, void 0, function* () {
    try {
        const user = yield exports.User.findOne({ username: username });
        if (!user)
            throw { msg: 'User Not Found' };
        return user;
    }
    catch (err) {
        console.error('[models/user/getByUsername]', err);
        throw err;
    }
});
const changePassword = (id, password) => __awaiter(this, void 0, void 0, function* () {
    try {
        const salt = yield bcrypt.genSalt(10);
        const hash = yield bcrypt.hash(password, salt);
        const res = yield exports.User.updateOne({ _id: id }, { $set: { password: hash } });
        return res;
    }
    catch (err) {
        console.log(err);
        throw err;
    }
});
const changeUsername = (id, username) => __awaiter(this, void 0, void 0, function* () {
    try {
        const res = yield exports.User.updateOne({ _id: id }, { $set: { username: username } });
        return res;
    }
    catch (err) {
        console.log(err);
        throw err;
    }
});
const comparePassword = (password, userPassword) => __awaiter(this, void 0, void 0, function* () {
    try {
        const isMatch = yield bcrypt.compare(password, userPassword);
        if (!isMatch)
            return false;
        return true;
    }
    catch (err) {
        console.log(err);
        throw err;
    }
});
const createUser = (newUser) => __awaiter(this, void 0, void 0, function* () {
    try {
        const salt = yield bcrypt.genSalt(10);
        const hash = yield bcrypt.hash(newUser.password, salt);
        newUser.password = hash;
        const user = yield newUser.save();
        return user;
    }
    catch (err) {
        console.error('[models/user/createUser]', err);
        throw err;
    }
});
const getById = (id) => __awaiter(this, void 0, void 0, function* () {
    try {
        const user = yield exports.User.findOne({ _id: id });
        if (!user)
            throw { msg: 'User Not Found' };
        return user;
    }
    catch (err) {
        console.error('[models/user/getById]', err);
        throw err;
    }
});
userSchema.methods.comparePassword = comparePassword;
userSchema.methods.changeUsername = changeUsername;
userSchema.methods.comparePassword = comparePassword;
userSchema.methods.createUser = createUser;
userSchema.methods.getById = getById;
userSchema.methods.getByUsername = getByUsername;
const user = db_1.default.model("User", userSchema);
exports.User = Object.assign(user, {
    changePassword: changePassword,
    changeUsername: changeUsername,
    comparePassword: comparePassword,
    createUser: createUser,
    getById: getById,
    getByUsername: getByUsername
});
