## user-service

A simple microservice managing user accounts, part of the *currently conceptual* potikreff.pl ecosystem.
It uses a RESTful API described with Postman.

##### [Documentation](https://documenter.getpostman.com/view/7313570/S1LySmKh)

---


In case You want to run the app on Your device, You need to have nodeJS v11+ and mongodb installed.
Installation steps:

1. Clone the repo to a directory of choice
2. Create a .env file in repo's root folder and paste in following content, replacing italic text with necessary data: <br/>
SECRET=*just a random string*<br/>
REISSUE_SECRET=*just a random string*<br/>
PASSWORD_RESET_SECRET=*just a random string*<br/>
MAIL_HOST=*mail.example.com*<br/>
MAIL_USER=*mail@example.com*<br/>
MAIL_PASSWORD=*password*<br/>
SERVER_PATH=*your local ip address (to be included in link for password recovery functionality), for example: http://192.168.1.1*<br/>
Optional enviromental variables:<br/>
PORT=*default is 3000*
3. Navigate to the projects directory in command line
4. Start the mangodb service<br/>
If You're running a debian-based distro type in: `sudo service mongod start`<br/>
If You're running an arch-based distro type in: `sudo systemctl start mongodb`<br/>
5. Run following commands: <br/>
`npm install` <br/>
`npm run start` <br/>
5. The app should be running on localhost:3000 or your port of choice.
6. Optionally you can start the app in watch mode running `npm run watch` or fire up the unit tests using `npm run test`.
You need to install the devDependencies to run the test. Also to edit the app in TypeScript you need to have the TypeScript command line tool
to be able to compile the code to js.

Warning: when using `npm install` node will automatically install the newest versions of the packages and also update the package.json file.
In case You experience any compabilty issues look up the initial version in package.json on github or just redownload the repo,
remove the '^' in incompatibile package version and run `npm update` / `npm install`
